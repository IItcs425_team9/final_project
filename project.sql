drop table Customer;
drop table Orders;
drop table Products;
drop table Warehouses;
drop table Address;
drop table StaffMember;
drop table Supplier;
drop table CreditCard;
drop table ProductPricing;
drop table stock;
drop table supplies;
drop table cotains;
drop table staff_address;
drop table customer_address;


create table Customer(
  CusID varchar(30)  not null,
  Lastname varchar(30)  not null,
  Firstname varchar(30) not null,
  Middlename varchar(30),
  balance numeric (8,2),
  Primary key(CusID)
);

create table Products(
  ProductID varchar(30)  not null,
  ProductName varchar(30)  not null,
  ProductType varchar(30)  not null,
  ProductSize INT,
  Information varchar(30),
  Primary key(ProductID)
);

create table  ProductPricing(
  ProductID varchar(30)  not null,
  states varchar(30)  not null,
  price numeric(8,2) not null,
  Primary key(ProductID,states),
  Foreign key(ProductID) references Products
);

create table Address(
  addressID varchar(30) not null,
  StreetNum varchar(30) not null,
  AptNo INT,
  city varchar(30) not null,
  states varchar(30) not null,
  zipcode numeric(5) not null,
  country varchar(30) not null,
  Primary key(addressID)
);

create table Warehouses(
  WarehouseID varchar(30) not null,
  Warehouse_capacity INT,
  addressID varchar(30) not null,
  Primary key(WarehouseID),
  Foreign key(addressID)references Address
);

create table Supplier(
  supplierID varchar(30) not null,
  Lastname varchar(30)  not null,
  Firstname varchar(30) not null,
  Middlename varchar(30),
  items varchar(30) not null,
  specfic_price numeric(8,2) not null,
  addressID varchar(30) not null,
  Primary key(supplierID),
  Foreign key(addressID)references Address
);

create table  CreditCard(
  cardNum INT not null,
  CusID varchar(30) not null,
  addressID varchar(30) not null,
  cvv numeric (3,0) not null,
  dueDate date,
  Primary key(cardNum),
  Foreign key(CusID) references Customer,
  Foreign key(addressID)references Address
);

create table Orders(
  orderID varchar(30) not null,
  quanitity INT not null,
  status varchar(30) not null,
  dates DATE,
  paymentID INT not null,
  CusID varchar(30)  not null,
  total numeric(8,2) not null,
  Primary key(orderID),
  Foreign key(CusID) references Customer,
  Foreign key(paymentID)references CreditCard
);

create table StaffMember(
  StaffID varchar(30) not null,
  Lastname varchar(30)  not null,
  Firstname varchar(30) not null,
  job_title varchar(30),
  salary numeric (8,2),
  Primary key(StaffID)
);

create table stock(
  ProductID varchar(30)  not null,
  WarehouseID varchar(30) not null,
  quanitity INT,
  Primary key(ProductID,WarehouseID),
  Foreign key(ProductID) references Products,
  Foreign key(WarehouseID) references Warehouses
);

create table supplies(
  ProductID varchar(30)  not null,
  supplierID varchar(30) not null,
  Primary key(ProductID,supplierID),
  Foreign key(ProductID) references Products,
  Foreign key(supplierID) references Supplier
);

create table cotains(
  ProductID varchar(30)  not null,
  orderID varchar(30) not null,
  Primary key(ProductID,orderID),
  Foreign key(ProductID) references Products,
  Foreign key(orderID) references Orders
);

create table staff_address(
  StaffID varchar(30) not null,
  addressID varchar(30) not null,
  Primary key(StaffID,addressID),
  Foreign key(StaffID) references StaffMember,
  Foreign key(addressID) references Address
);

create table customer_address(
  CusID varchar(30)  not null,
  addressID varchar(30) not null,
  Primary key(CusID,addressID),
  Foreign key(CusID) references Customer,
  Foreign key(addressID) references Address
);















package model;

public class Address {
	private String StreetNum;
	private String AptNo;
	private String city;
	private String states;
	private String country;
	private String zipcode;
	private String addressID;

	public String getAddressID() {
		return addressID;
	}

	public void setAddressID(String addressID) {
		this.addressID = addressID;
	}

	public String getStreetNum() {
		return StreetNum;
	}

	public void setStreetNum(String streetNum) {
		StreetNum = streetNum;
	}

	public String getAptNo() {
		return AptNo;
	}

	public void setAptNo(String aptNo) {
		AptNo = aptNo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStates() {
		return states;
	}

	public void setStates(String states) {
		this.states = states;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	
}

package Dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class CreditCardDao {
	static final String jdbcURL="jdbc:oracle:thin:@fourier.cs.iit.edu:1521:orcl";
    static final String User_name="yxu109";
    static final String password="xyf19920802";
    
    public void addCreditCard(){
      	 try{ //STEP 2: Register JDBC driver
      		 Scanner keyboard = new Scanner(System.in);
      		    System.out.print("Please enter cardNum : ");
      		    String cardNum= keyboard.next();
      		    System.out.print("Please enter CusID: ");
      		    String CusID = keyboard.next();
      		    System.out.print("Please enter addressID: ");
      		    String addressID = keyboard.next();
      		    System.out.print("Please Enter cvv: ");
      		    String cvv = keyboard.next();
      		    System.out.print("Please Enter dueDate: The date style is mm/yy");
      		    String dueDate = keyboard.next();
      		
       
           Class.forName("oracle.jdbc.driver.OracleDriver");

           //STEP 3: Open a connection
           System.out.println("Connecting to a selected database...");
           Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
           System.out.println("Connected database successfully...");
           
                 //STEP 4: Execute a query
           System.out.println("Creating statement...");
           Statement stmt = conn.createStatement();
         try{
      	   String sql="Insert into CreditCard values('"+cardNum+"','"+CusID+"',"
      	   		+ "'"+addressID+"','"+cvv+"','"+dueDate+"')";
      	  stmt.executeUpdate(sql);
      	  System.out.println("insert successfully!");
        }catch(SQLException sqle){
      	  System.out.println("Couldn't insert tuple."+sqle);
        }
       }catch(Exception sqle){
          	System.out.println("Exception:"+sqle);
      }
     }
    
    public void delCreditCard(){
   	 try{ //STEP 2: Register JDBC driver
   		 
   		Scanner keyboard = new Scanner(System.in);
		    System.out.print("Please enter CardNum : ");
		    String CardNum= keyboard.next();
		    
        Class.forName("oracle.jdbc.driver.OracleDriver");

        //STEP 3: Open a connection
        System.out.println("Connecting to a selected database...");
        Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
        System.out.println("Connected database successfully...");
        
              //STEP 4: Execute a query
        System.out.println("Creating statement...");
        Statement stmt = conn.createStatement();
      try{
   	   String sql="Delete from CreditCard where CardNum="+CardNum;
   	  stmt.executeUpdate(sql);
   	  System.out.println("delete successfully!");
     }catch(SQLException sqle){
   	  System.out.println("Couldn't delete tuple."+sqle);
     }
     
       }catch(Exception sqle){
       	System.out.println("Exception:"+sqle);
       }
     
   
    } 
    
    
    public void updateCreditCard(){
   	 try{ //STEP 2: Register JDBC driver
   		 
   		Scanner keyboard = new Scanner(System.in);
		    System.out.print("Please enter cardNum : ");
		    String cardNum= keyboard.next();
		    System.out.print("Please Enter cvv: ");
		    String cvv = keyboard.next();
		    System.out.print("Please Enter dueDate: The date style is mm/yy");
		    String dueDate = keyboard.next();
		
		
		    
        Class.forName("oracle.jdbc.driver.OracleDriver");

        //STEP 3: Open a connection
        System.out.println("Connecting to a selected database...");
        Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
        System.out.println("Connected database successfully...");
        
              //STEP 4: Execute a query
        System.out.println("Creating statement...");
        Statement stmt = conn.createStatement();
      try{
   	   String sql="Update CreditCard set cvv='"+cvv+"',dueDate='"+dueDate+"' where cardNum='"+cardNum+"'";
   	  stmt.executeUpdate(sql);
   	  System.out.println("update successfully!");
     }catch(SQLException sqle){
   	  System.out.println("Couldn't update tuple."+sqle);
     }
       }catch(Exception sqle){
       	System.out.println("Exception:"+sqle);
       }
    }
    
    
    
    public void queryCreditCard(){
   	 try{ //STEP 2: Register JDBC driver
		    
        Class.forName("oracle.jdbc.driver.OracleDriver");

        //STEP 3: Open a connection
        System.out.println("Connecting to a selected database...");
        Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
        System.out.println("Connected database successfully...");
        
              //STEP 4: Execute a query
        System.out.println("Creating statement...");
        Statement stmt = conn.createStatement();
       
       System.out.println("Here are all cards' information!");
       ResultSet rset=stmt.executeQuery("select * from CreditCard");
       while(rset.next()){
       	System.out.println("cardNum: "+rset.getString("cardNum")+", CusID: "+rset.getString("CusID")+",addressID: "
             +rset.getString("addressID")+", cvv: "+rset.getString("cvv")+", dueDate: "+rset.getString("dueDate"));
       }
     
       }catch(Exception sqle){
       	System.out.println("Exception:"+sqle);
       }
    } 
    
    
    public void getCreditCard(){
      	 try{ //STEP 2: Register JDBC driver
      	   Scanner keyboard = new Scanner(System.in);
     	   System.out.print("Please enter CustID : ");
  		   String CusID= keyboard.next();
           Class.forName("oracle.jdbc.driver.OracleDriver");

           //STEP 3: Open a connection
           System.out.println("Connecting to a selected database...");
           Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
           System.out.println("Connected database successfully...");
           
                 //STEP 4: Execute a query
           System.out.println("Creating statement...");
           Statement stmt = conn.createStatement();
          
          System.out.println("Here is the cards' information!");
          ResultSet rset=stmt.executeQuery("select * from CreditCard where CusID ="+CusID);
          while(rset.next()){
        		System.out.println("cardNum: "+rset.getString("cardNum")+", CusID: "+rset.getString("CusID")+",addressID: "
        	             +rset.getString("addressID")+", cvv: "+rset.getString("cvv")+", dueDate: "+rset.getString("dueDate"));
          }
        
          }catch(Exception sqle){
          	System.out.println("Exception:"+sqle);
          }
       } 
    
    
    
}

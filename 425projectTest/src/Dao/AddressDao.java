package Dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class AddressDao {
	static final String jdbcURL="jdbc:oracle:thin:@fourier.cs.iit.edu:1521:orcl";
    static final String User_name="yxu109";
    static final String password="xyf19920802";
	
	
    public void addAddress(){
   	 try{ //STEP 2: Register JDBC driver
   		 Scanner keyboard = new Scanner(System.in);
   		    System.out.print("Please enter addressID : ");
   		    String addressID= keyboard.next();
   		    System.out.print("Please enter streetNum: ");
   		    String streetNum = keyboard.next();
   		    System.out.print("Please enter AptNo: ");
   		    String AptNo = keyboard.next();
   		    System.out.print("Please Enter city: ");
   		    String city = keyboard.next();
   		    System.out.print("Please Enter states: ");
   		    String states = keyboard.next();
   		    System.out.print("Please Enter zipcode: ");
		    String zipcode = keyboard.next();
		    System.out.print("Please Enter country: ");
		    String country = keyboard.next();
    
        Class.forName("oracle.jdbc.driver.OracleDriver");

        //STEP 3: Open a connection
        System.out.println("Connecting to a selected database...");
        Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
        System.out.println("Connected database successfully...");
        
              //STEP 4: Execute a query
        System.out.println("Creating statement...");
        Statement stmt = conn.createStatement();
      try{
   	   String sql="Insert into Address values('"+addressID+"','"+streetNum+"',"
   	   		+ "'"+AptNo+"','"+city+"','"+states+"','"+zipcode+"','"+country+"')";
   	  stmt.executeUpdate(sql);
   	  System.out.println("add successfully!");
     }catch(SQLException sqle){
   	  System.out.println("Couldn't insert tuple."+sqle);
     }
    }catch(Exception sqle){
       	System.out.println("Exception:"+sqle);
   }
  }  
    
    
    
    public void delAddress(){
   	 try{ //STEP 2: Register JDBC driver
   		 
   		Scanner keyboard = new Scanner(System.in);
		    System.out.print("Please enter addressID : ");
		    String addressID= keyboard.next();
		    
        Class.forName("oracle.jdbc.driver.OracleDriver");

        //STEP 3: Open a connection
        System.out.println("Connecting to a selected database...");
        Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
        System.out.println("Connected database successfully...");
        
              //STEP 4: Execute a query
        System.out.println("Creating statement...");
        Statement stmt = conn.createStatement();
      try{
   	   String sql="Delete from Address where addressID="+addressID;
   	  stmt.executeUpdate(sql);
   	  System.out.println("delete successfully!");
     }catch(SQLException sqle){
   	  System.out.println("Couldn't delete tuple."+sqle);
     }
       /*ResultSet rset=stmt.executeQuery(
       		"select CusID,balance from Customer");
       while(rset.next()){
       	System.out.println(rset.getString("CusID")+","+rset.getString("balance"));
       }*/
     
       }catch(Exception sqle){
       	System.out.println("Exception:"+sqle);
       }
    
    }  

    public void updateAddress(){
   	 try{ //STEP 2: Register JDBC driver
   		Scanner keyboard = new Scanner(System.in);
   		System.out.print("Please enter addressID : ");
		    String addressID= keyboard.next();
		    System.out.print("Please enter streetNum ");
		    String streetNum = keyboard.next();
		    System.out.print("Please enter AptNo: ");
		    String AptNo = keyboard.next();
		    System.out.print("Please Enter city: ");
		    String city = keyboard.next();
		    System.out.print("Please Enter states: ");
		    String states = keyboard.next();
		    System.out.print("Please Enter zipcode: ");
	    String zipcode = keyboard.next();
	    System.out.print("Please Enter country: ");
	    String country = keyboard.next();
		    
        Class.forName("oracle.jdbc.driver.OracleDriver");

        //STEP 3: Open a connection
        System.out.println("Connecting to a selected database...");
        Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
        System.out.println("Connected database successfully...");
        
              //STEP 4: Execute a query
        System.out.println("Creating statement...");
        Statement stmt = conn.createStatement();
      try{
   	   String sql="Update Address set streetNum='"+streetNum+"',AptNo='"+AptNo+"',city='"+city+"',states='"+states+"', zipcode='"+zipcode+"',country='"+country+"' where addressID='"+addressID+"'";
   	  stmt.executeUpdate(sql);
   	  System.out.println("update successfully!");
     }catch(SQLException sqle){
   	  System.out.println("Couldn't update tuple."+sqle);
     }
       /*ResultSet rset=stmt.executeQuery(
       		"select CusID,balance from Customer");
       while(rset.next()){
       	System.out.println(rset.getString("CusID")+","+rset.getString("balance"));
       }*/
     
       }catch(Exception sqle){
       	System.out.println("Exception:"+sqle);
       }
    }  
    
    public void queryAddress(){
   	 try{ //STEP 2: Register JDBC driver
		    
        Class.forName("oracle.jdbc.driver.OracleDriver");

        //STEP 3: Open a connection
        System.out.println("Connecting to a selected database...");
        Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
        System.out.println("Connected database successfully...");
        
              //STEP 4: Execute a query
        System.out.println("Creating statement...");
        Statement stmt = conn.createStatement();
       
       System.out.println("Here are all Address's information!");
       ResultSet rset=stmt.executeQuery("select * from Address");
       while(rset.next()){
       	System.out.println("addressID: "+rset.getString("addressID")+", streetNum: "+rset.getString("streetNum")+", AptNo: "
             +rset.getString("AptNo")+", city: "+rset.getString("city")+", states: "+rset.getString("states")+", zipcode: "+rset.getString("zipcode")+", country: "+rset.getString("country"));
       }
     
       }catch(Exception sqle){
       	System.out.println("Exception:"+sqle);
       }
    }  
    
    public void getAddress(){
      	 try{ //STEP 2: Register JDBC driver
   		    
      	Scanner keyboard = new Scanner(System.in);
      	System.out.print("Please enter addressID : ");
   	    String addressID= keyboard.next();
           Class.forName("oracle.jdbc.driver.OracleDriver");

           //STEP 3: Open a connection
           System.out.println("Connecting to a selected database...");
           Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
           System.out.println("Connected database successfully...");
           
                 //STEP 4: Execute a query
           System.out.println("Creating statement...");
           Statement stmt = conn.createStatement();
          
          System.out.println("Here is the addressID's information!");
          ResultSet rset=stmt.executeQuery("select * from Address where addressID="+addressID);
          while(rset.next()){
        		System.out.println("addressID: "+rset.getString("addressID")+", streetNum: "+rset.getString("streetNum")+", AptNo: "
        	             +rset.getString("AptNo")+", city: "+rset.getString("city")+", states: "+rset.getString("states")+", zipcode: "+rset.getString("zipcode")+", country: "+rset.getString("country"));
          }
        
          }catch(Exception sqle){
          	System.out.println("Exception:"+sqle);
          }
       }  
    
    
    
	
}

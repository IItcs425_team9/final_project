package Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import model.Address;
import model.Owncard;


public class OrderDao {
	static final String jdbcURL="jdbc:oracle:thin:@fourier.cs.iit.edu:1521:orcl";
    static final String User_name="yxu109";
    static final String password="xyf19920802";
    
 
    
    public void createOrder() throws Exception{
      	  //STEP 2: Register JDBC driver
      		Scanner keyboard = new Scanner(System.in);
      		    System.out.print("Please enter orderID : ");
      		    String orderID= keyboard.nextLine();
      		    System.out.print("Please enter quanitity : ");
      		    int quanitity =Integer.parseInt(keyboard.nextLine());
      		    System.out.print("Please enter productID : ");
    		    String productID = keyboard.nextLine();
      		    System.out.print("Please enter status : ");
      		    String status = keyboard.nextLine();
      		    System.out.print("Please Enter dates : The date type is mm/dd/yy ");
      		    String dates = keyboard.nextLine();
   		        System.out.print("Please Enter CusID : ");
      		    String CusID = keyboard.nextLine();
      		  
      		   
       
           Class.forName("oracle.jdbc.driver.OracleDriver");

           //STEP 3: Open a connection
           System.out.println("Connecting to a selected database...");
           Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
           System.out.println("Connected database successfully...");
           
                 //STEP 4: Execute a query
           System.out.println("Creating statement...");
        
          
         try{
        	 int total = 0;
        String sql=" Insert into Orders values (?,?,?,?,?,?,?,?) ";
          PreparedStatement ptmt = conn.prepareStatement(sql);
          
        	String sql1="select CardNum "
 		 			+"from Customer NATURAL JOIN CreditCard "	
 		 		    +"where CusID="+CusID+"";
        	PreparedStatement query = conn.prepareStatement(sql1);   
            ResultSet rset1=query.executeQuery();
            
            String sql2=" select price from ProductPricing where ProductID = ? ";
           
            
	           PreparedStatement price = conn.prepareStatement(sql2); 
	           price.setString(1, productID );
	           ResultSet rset2=price.executeQuery();
          while(rset2.next()){
        	 total=(rset2.getInt("price"))*quanitity;
        	 String sql3=" update Customer set balance = balance+"+total+" where CusID ="+CusID+"";
        	 Statement balance=conn.createStatement();
             balance.executeUpdate(sql3);
         }
      
         
           
          if(rset1.next()){
           ptmt.setString(1, orderID);
           ptmt.setInt(2, quanitity);
           ptmt.setString(3, productID);
           ptmt.setString(4, status);
           ptmt.setString(5, dates);
           ptmt.setString(6, rset1.getString(1));
           ptmt.setString(7, CusID);
           ptmt.setInt(8, total);
           
           ptmt.executeUpdate();
           System.out.println("Order creates successfully!");
           }else{
        	   System.out.println("create unsuccessfully!");
           }
        	   	     	  
        }catch(SQLException sqle){
      	  System.out.println("Couldn't insert tuple."+sqle);
      }
       
  } 
    
    public void delOrder(){
      	 try{ //STEP 2: Register JDBC driver
      		 
      		Scanner keyboard = new Scanner(System.in);
   		    System.out.print("Please enter orderID : ");
   		    String orderID= keyboard.next();
   		    
           Class.forName("oracle.jdbc.driver.OracleDriver");

           //STEP 3: Open a connection
           System.out.println("Connecting to a selected database...");
           Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
           System.out.println("Connected database successfully...");
           
                 //STEP 4: Execute a query
           System.out.println("Creating statement...");
           Statement stmt = conn.createStatement();
         try{
      	   String sql="Delete from Orders where orderID="+orderID;
      	  stmt.executeUpdate(sql);
      	  System.out.println("delete order successfully!");
        }catch(SQLException sqle){
      	  System.out.println("Couldn't delete tuple."+sqle);
        }
          }catch(Exception sqle){
          	System.out.println("Exception:"+sqle);
          }   
    }    
   
    
    public void queryOrder(){
     	 try{ //STEP 2: Register JDBC driver
  		    
          Class.forName("oracle.jdbc.driver.OracleDriver");

          //STEP 3: Open a connection
          System.out.println("Connecting to a selected database...");
          Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
          System.out.println("Connected database successfully...");
          
                //STEP 4: Execute a query
          System.out.println("Creating statement...");
          Statement stmt = conn.createStatement();
        try{
        	 
            System.out.println("Here are all orders' information!");
            ResultSet rset=stmt.executeQuery("select * from Orders");
            while(rset.next()){
            	System.out.println("orderID: "+rset.getString("orderID")+" ,quanitity: "+rset.getInt("quanitity")+" ,productID: "
                  +rset.getString("productID")+" ,status: "+rset.getString("status")+" ,dates: "+rset.getString("dates")+" ,paymentID: "
            	  +rset.getString("paymentID")+" ,CusID: "+rset.getString("CusID")+" ,total: "+rset.getInt("total"));
            }
       }catch(SQLException sqle){
     	  System.out.println("Couldn't query tuple."+sqle);
       }
         }catch(Exception sqle){
         	System.out.println("Exception:"+sqle);
         }   
   }    
    
    public void getOrder(){
    	 try{ //STEP 2: Register JDBC driver
    		 
    		Scanner keyboard = new Scanner(System.in);
 		    System.out.print("Please enter CusID : ");
 		    String CusID= keyboard.next();
 		    
         Class.forName("oracle.jdbc.driver.OracleDriver");

         //STEP 3: Open a connection
         System.out.println("Connecting to a selected database...");
         Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
         System.out.println("Connected database successfully...");
         
               //STEP 4: Execute a query
         System.out.println("Creating statement...");
         Statement stmt = conn.createStatement();
       try{
       	 
           System.out.println("Here are the orders' information!");
           ResultSet rset=stmt.executeQuery("select * from Orders where CusID="+CusID);
           while(rset.next()){
           	System.out.println("orderID: "+rset.getString("orderID")+" ,quanitity: "+rset.getInt("quanitity")+" ,productID: "
                 +rset.getString("productID")+" ,status: "+rset.getString("status")+" ,dates: "+rset.getString("dates")+" ,paymentID: "
           	  +rset.getString("paymentID")+" ,CusID: "+rset.getString("CusID")+" ,total: "+rset.getInt("total"));
           }
      }catch(SQLException sqle){
    	  System.out.println("Couldn't get order."+sqle);
      }
        }catch(Exception sqle){
        	System.out.println("Exception:"+sqle);
        }   
  } 
  
    public void updateOrders(){
      	 try{ //STEP 2: Register JDBC driver
      		 
      		Scanner keyboard = new Scanner(System.in);
      		System.out.print("Please enter orderID : ");
   		    String orderID= keyboard.next();
   		    System.out.print("Please enter quanitity: ");
		    String quanitity = keyboard.next();
   		    System.out.print("Please enter status: ");
   		    String status = keyboard.next();
   		  
   		    
           Class.forName("oracle.jdbc.driver.OracleDriver");

           //STEP 3: Open a connection
           System.out.println("Connecting to a selected database...");
           Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
           System.out.println("Connected database successfully...");
           
                 //STEP 4: Execute a query
           System.out.println("Creating statement...");
           Statement stmt = conn.createStatement();
         try{
      	   String sql="Update Orders set status='"+status+"', quanitity="+quanitity+"  where orderID='"+orderID+"'";
      	  stmt.executeUpdate(sql);
      	  System.out.println("update successfully!");
        }catch(SQLException sqle){
      	  System.out.println("Couldn't update order."+sqle);
        }
      
          }catch(Exception sqle){
          	System.out.println("Exception:"+sqle);
          }
       }   
      
    
    public void testOrder() throws Exception{
    	  //STEP 2: Register JDBC driver
    		Scanner keyboard = new Scanner(System.in);
    		    System.out.print("Please enter orderID : ");
    		    String orderID= keyboard.nextLine();
    		    System.out.print("Please enter quanitity : ");
    		    int quanitity =Integer.parseInt(keyboard.nextLine());
    		    System.out.print("Please enter productID : ");
  		        String productID = keyboard.nextLine();
    		    System.out.print("Please enter status : ");
    		    String status = keyboard.nextLine();
    		    System.out.print("Please Enter dates : The date type is mm/dd/yy ");
    		    String dates = keyboard.nextLine();
 		        System.out.print("Please Enter CusID : ");
    		    String CusID = keyboard.nextLine();
    		    System.out.print("Please enter total : ");
    		    int total =Integer.parseInt(keyboard.nextLine());
    		   
     
         Class.forName("oracle.jdbc.driver.OracleDriver");

         //STEP 3: Open a connection
         System.out.println("Connecting to a selected database...");
         Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
         System.out.println("Connected database successfully...");
         
               //STEP 4: Execute a query
         System.out.println("Creating statement...");
      
        
       try{
    
      String sql=" Insert into Orders values (?,?,?,?,?,?,?,?) ";
        PreparedStatement ptmt = conn.prepareStatement(sql);
        
      	String sql1="select CardNum "
		 			+"from Customer NATURAL JOIN CreditCard "	
		 		    +"where CusID="+CusID+"";
      	PreparedStatement query = conn.prepareStatement(sql1);   
          ResultSet rset1=query.executeQuery();
                  
        while(rset1.next()){
         ptmt.setString(1, orderID);
         ptmt.setInt(2, quanitity);
         ptmt.setString(3, productID);
         ptmt.setString(4, status);
         ptmt.setString(5, dates);
         ptmt.setString(6, rset1.getString(1));
         ptmt.setString(7, CusID);
         ptmt.setInt(8, total);   
         ptmt.executeUpdate();
         System.out.println("Order creates successfully!");
         }
      	   	     	  
      }catch(SQLException sqle){
    	  System.out.println("Couldn't insert tuple."+sqle);
    }
     
       
       
       
       
   } 
   /*public int addCard(
            String cardNo,
            String expdate,
            String holdername,
            String cvv,
            String type,
            String stname,
            int stno,
            String aptno,
            String city,
            String zipcode
               ) throws SQLException{
    	

        Class.forName("oracle.jdbc.driver.OracleDriver");

        //STEP 3: Open a connection
        System.out.println("Connecting to a selected database...");
        Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
        System.out.println("Connected database successfully...");
        
       PreparedStatement incd = conn.prepareStatement("insert into credit_card values(?, ?, ?, ?, ?, ?)");
            incd.setString(1, cardNo);
            incd.setString(2, expdate);
            incd.setString(3, holdername);
            incd.setString(4, cvv);
            incd.setString(5, type);
         PreparedStatement quadd = conn.prepareStatement("select addressID "
                                                                                                                                                                        + "from address "
                                                                                                                                                                        + "where zipcode = ? "
                                                                                                                                                                        + "and st_no = ? "
                                                                                                                                                                        + "and st_name = ? "
                                                                                                                                                                        + "and apt_no = ?");
                    quadd.setString(1, zipcode);
                    quadd.setInt(2, stno);
                    quadd.setString(3, stname);
                    quadd.setString(4, aptno);
                  ResultSet rs = quadd.executeQuery();
              if (rs.next()){
            incd.setString(6, rs.getString(1));

               } else {
            AddressHandler addaddr = new AddressHandler(connDB);
            addaddr.addAddr(stname, zipcode, city, stno, aptno);
            incd.setString(6, addaddr.addrID);
        }
               return incd.executeUpdate();
           
  }*/
    
    /*public void createOrder() throws Exception{
    	  //STEP 2: Register JDBC driver
    		Scanner keyboard = new Scanner(System.in);
    		    System.out.print("Please enter orderID : ");
    		    String orderID= keyboard.nextLine();
    		    System.out.print("Please enter quanitity : ");
    		    int quanitity =Integer.parseInt(keyboard.nextLine());
    		    System.out.print("Please enter productID : ");
  		    String productID = keyboard.nextLine();
    		    System.out.print("Please enter status : ");
    		    String status = keyboard.nextLine();
    		    System.out.print("Please Enter dates : The date type is mm/dd/yy ");
    		    String dates = keyboard.nextLine();
 		        System.out.print("Please Enter CusID : ");
    		    String CusID = keyboard.nextLine();
    		  
    		   
     
         Class.forName("oracle.jdbc.driver.OracleDriver");

         //STEP 3: Open a connection
         System.out.println("Connecting to a selected database...");
         Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
         System.out.println("Connected database successfully...");
         
               //STEP 4: Execute a query
         System.out.println("Creating statement...");
      
        
       try{
      	 int total = 0;
      String sql=" Insert into Orders values (?,?,?,?,?,?,?,?) ";
        PreparedStatement ptmt = conn.prepareStatement(sql);
        
      	String sql1="select CardNum "
		 			+"from Customer NATURAL JOIN CreditCard "	
		 		    +"where CusID="+CusID+"";
      	PreparedStatement query = conn.prepareStatement(sql1);   
          ResultSet rset1=query.executeQuery();
          
          String sql2=" select price from ProductPricing where ProductID = ? ";
		    
	           PreparedStatement price = conn.prepareStatement(sql2); 
	           price.setString(1, productID );
	           ResultSet rset2=price.executeQuery();
        while(rset2.next()){
      	 total=(rset2.getInt("price"))*quanitity;
       }
            
         
        if(rset1.next()){
         ptmt.setString(1, orderID);
         ptmt.setInt(2, quanitity);
         ptmt.setString(3, productID);
         ptmt.setString(4, status);
         ptmt.setString(5, dates);
         ptmt.setString(6, rset1.getString(1));
         ptmt.setString(7, CusID);
         ptmt.setInt(8, total);
         
         ptmt.executeUpdate();
         System.out.println("Order creates successfully!");
         }else{
      	   System.out.println("create unsuccessfully!");
         }
      	   	     	  
      }catch(SQLException sqle){
    	  System.out.println("Couldn't insert tuple."+sqle);
    }
     
} */
  
}

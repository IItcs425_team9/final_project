package Dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;



public class CustomerDao {
	
		
	static final String jdbcURL="jdbc:oracle:thin:@fourier.cs.iit.edu:1521:orcl";
    static final String User_name="yxu109";
    static final String password="xyf19920802";

   
    
     public void delCustomer(){
    	 try{ //STEP 2: Register JDBC driver
    		 
    		Scanner keyboard = new Scanner(System.in);
 		    System.out.print("Please enter CusID : ");
 		    String CusID= keyboard.next();
 		    
         Class.forName("oracle.jdbc.driver.OracleDriver");

         //STEP 3: Open a connection
         System.out.println("Connecting to a selected database...");
         Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
         System.out.println("Connected database successfully...");
         
               //STEP 4: Execute a query
         System.out.println("Creating statement...");
         Statement stmt = conn.createStatement();
       try{
    	   String sql="Delete from Customer where CusID="+CusID;
    	  stmt.executeUpdate(sql);
    	  System.out.println("delete successfully!");
      }catch(SQLException sqle){
    	  System.out.println("Couldn't delete tuple."+sqle);
      }
       
        }catch(Exception sqle){
        	System.out.println("Exception:"+sqle);
        }
      
    
     }  

     
     
     
     public void addCustomer(){
    	 try{ //STEP 2: Register JDBC driver
    		 Scanner keyboard = new Scanner(System.in);
    		    System.out.print("Please enter CusID : ");
    		    String CusID= keyboard.next();
    		    System.out.print("Please enter Lastname ");
    		    String Lastname = keyboard.next();
    		    System.out.print("Please enter Firstname: ");
    		    String Firstname = keyboard.next();
    		    System.out.print("Please Enter Middlename: ");
    		    String Middlename = keyboard.next();
    		    System.out.print("Please Enter balance: ");
    		    int balance = keyboard.nextInt();
    		   
     
         Class.forName("oracle.jdbc.driver.OracleDriver");

         //STEP 3: Open a connection
         System.out.println("Connecting to a selected database...");
         Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
         System.out.println("Connected database successfully...");
         
               //STEP 4: Execute a query
         System.out.println("Creating statement...");
         Statement stmt = conn.createStatement();
       try{
    	   String sql="Insert into Customer values('"+CusID+"','"+Lastname+"',"
    	   		+ "'"+Firstname+"','"+Middlename+"',"+balance+")";
    	  stmt.executeUpdate(sql);
    	  System.out.println("insert successfully!");
      }catch(SQLException sqle){
    	  System.out.println("Couldn't insert tuple."+sqle);
      }
     }catch(Exception sqle){
        	System.out.println("Exception:"+sqle);
    }
   }  
     
     public void updateCustomer(){
    	 try{ //STEP 2: Register JDBC driver
    		 
    		Scanner keyboard = new Scanner(System.in);
    		System.out.print("Please enter CusID : ");
 		    String CusID= keyboard.next();
 		    System.out.print("Please enter Firstname ");
 		    String Firstname = keyboard.next();
 		    System.out.print("Please enter MIiddlename: ");
 		    String Middlename = keyboard.next();
 		    System.out.print("Please Enter Lastname: ");
 		    String Lastname = keyboard.next();
 		    System.out.print("Please Enter balance: ");
 		    int balance = keyboard.nextInt();
 		    
         Class.forName("oracle.jdbc.driver.OracleDriver");

         //STEP 3: Open a connection
         System.out.println("Connecting to a selected database...");
         Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
         System.out.println("Connected database successfully...");
         
               //STEP 4: Execute a query
         System.out.println("Creating statement...");
         Statement stmt = conn.createStatement();
       try{
    	   String sql="Update Customer set Lastname='"+Lastname+"',Firstname='"+Firstname+"',Middlename='"+Middlename+"',balance="+balance+" where CusID='"+CusID+"'";
    	  stmt.executeUpdate(sql);
    	  System.out.println("update successfully!");
      }catch(SQLException sqle){
    	  System.out.println("Couldn't update tuple."+sqle);
      }
        /*ResultSet rset=stmt.executeQuery(
        		"select CusID,balance from Customer");
        while(rset.next()){
        	System.out.println(rset.getString("CusID")+","+rset.getString("balance"));
        }*/
      
        }catch(Exception sqle){
        	System.out.println("Exception:"+sqle);
        }
     }  
     

     public void queryCustomer(){
    	 try{ //STEP 2: Register JDBC driver
 		    
         Class.forName("oracle.jdbc.driver.OracleDriver");

         //STEP 3: Open a connection
         System.out.println("Connecting to a selected database...");
         Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
         System.out.println("Connected database successfully...");
         
               //STEP 4: Execute a query
         System.out.println("Creating statement...");
         Statement stmt = conn.createStatement();
        
        System.out.println("Here are all customer's information!");
        ResultSet rset=stmt.executeQuery("select * from Customer");
        while(rset.next()){
        	System.out.println(rset.getString("CusID")+","+rset.getString("Firstname")+","
              +rset.getString("Middlename")+","+rset.getString("Lastname")+","+rset.getInt("balance"));
        }
      
        }catch(Exception sqle){
        	System.out.println("Exception:"+sqle);
        }
     }  
     
     public void getCustomer(){
    	 try{ //STEP 2: Register JDBC driver
    		 
    	    Scanner keyboard = new Scanner(System.in);
     		System.out.print("Please enter CusID : ");
  		    String CusID= keyboard.next();
  		    
         Class.forName("oracle.jdbc.driver.OracleDriver");

         //STEP 3: Open a connection
         System.out.println("Connecting to a selected database...");
         Connection conn = DriverManager.getConnection(jdbcURL,User_name, password);
         System.out.println("Connected database successfully...");
         
               //STEP 4: Execute a query
         System.out.println("Creating statement...");
         Statement stmt = conn.createStatement();
        
        System.out.println("Here is the customer's information!");
        ResultSet rset=stmt.executeQuery("select * from Customer where CusID="+CusID);
        while(rset.next()){
        	System.out.println(rset.getString("CusID")+","+rset.getString("Firstname")+","
              +rset.getString("Middlename")+","+rset.getString("Lastname")+","+rset.getInt("balance"));
        }
      
        }catch(Exception sqle){
        	System.out.println("Exception:"+sqle);
        }
     } 
     
}

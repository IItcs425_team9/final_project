package view;

import java.util.Scanner;

import action.addressAction;
import action.creditcardAction;
import action.orderAction;
import action.priceAction;
import action.productAction;
import action.stockAction;

public class view {

	public static void main(String[] args) throws Exception {
		  // TODO Auto-generated method stub
      view v=new view();
      v.run();
		 
	}

	private void run() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("***********************Main Menu****************************");
		System.out.println("please input");
		System.out.println("1: customer log in");
		System.out.println("2: staff log in");
		System.out.println("3: exit!");
		System.out.println("************************************************************");
		Scanner keyboard = new Scanner(System.in);
		int a=keyboard.nextInt();
		switch (a) {
		case 1:
			
			costomer();
			break;
		case 2:
		
		     staff();  
		     break;
		case 3:
			System.out.println("exit successfully!");
			break;
		default:
			break;
		}
	}

	private void staff() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("************************************************************");
		System.out.println("please choose");
		System.out.println("1: update product");
		System.out.println("2: delete product");
		System.out.println("3: add product");
		System.out.println("4: update price");
		System.out.println("5: query product");
		System.out.println("6: manage stock");
		System.out.println("7: log out");
		System.out.println("************************************************************");
		Scanner keyboard = new Scanner(System.in);
		int a=keyboard.nextInt();
		// TODO Auto-generated method stub
		switch (a) {
		case 1:
			update();
			staff();
			break;
		case 2:
			delproduct();
			staff();
			break;
		case 3:
			addProduct();
			staff();
			break;
		case 4:
			updatePrice();
			staff();
			break;
		case 5:
		    queryProduct();
			staff();
			break;
		case 6:
			System.out.println("please choose function");
			System.out.println("1: manage stock");
			System.out.println("2: query stock");
			System.out.println("any other key to return!");
			Scanner press3 = new Scanner(System.in);
			int g=press3.nextInt();
			if(g==1){
				updateStock();
			}
			if(g==2){
				queryStock();
			}
			staff();
			break;
		case 7:
			run();
		default:
			break;
		}
	}

	private void queryStock() {
		// TODO Auto-generated method stub
		stockAction s=new stockAction();
		s.queryStock();
	}

	private void updateStock() {
		// TODO Auto-generated method stub
		stockAction s=new stockAction();
		s.updateStock();
	}

	private void queryProduct() {
		// TODO Auto-generated method stub
		productAction p=new productAction();
		p.query();
	}

	private void updatePrice() {
		// TODO Auto-generated method stub
		priceAction p=new priceAction();
		p.update();
	}

	private void addProduct() {
		// TODO Auto-generated method stub
		productAction p=new productAction();
		p.add();
	}

	private void delproduct() {
		// TODO Auto-generated method stub
		productAction p=new productAction();
		p.del();
	}

	private void update() {
		// TODO Auto-generated method stub
		productAction p=new productAction();
		p.update();
		
	}

	private void costomer() throws Exception {
		System.out.println("************************************************************");
		System.out.println("please choose function");
		System.out.println("1: Manage creditcard");
		System.out.println("2: Manage address");
		System.out.println("3: Scan product");
		System.out.println("4: Purchase product");
		System.out.println("5: Shopping cart");
		System.out.println("6: My order");
		System.out.println("7: log out");
		System.out.println("************************************************************");	
		Scanner keyboard = new Scanner(System.in);
		int a=keyboard.nextInt();
		// TODO Auto-generated method stub
		switch (a) {
		case 1:
			System.out.println("Please choose function!");
			System.out.println("1: add creditcard");
			System.out.println("2: delete creditcard");
			System.out.println("3: update creditcard");
			System.out.println("4: query creditcard");
			System.out.println("any other key to return!");
			Scanner press1 = new Scanner(System.in);
			int g=press1.nextInt();
			if(g==1){
				addcard();
			}
			if(g==2){
				delcard();
			}
			if(g==3){
			    updatecard();
			}
			if(g==4){
			    querycard();
			}
			costomer();
			break;
		case 2:
			System.out.println("Please choose funtion!");
			System.out.println("1: add address");
			System.out.println("2: delete address");
			System.out.println("3: update address");
			System.out.println("4: query address");
			System.out.println("any other key to return!");
			Scanner press2 = new Scanner(System.in);
			int f=press2.nextInt();
			if(f==1){
				addaddress();
			}
			if(f==2){
				deladdress();
			}
			if(f==3){
			    updateaddress();
			}
			if(f==4){
			    queryaddress();
			}
			costomer();
			break;
		case 3:
			Scan();
			costomer();
			break;
		case 4:
			Purchase();
			costomer();		
			break;
		case 5:
			getOrder();
			System.out.println("Please press '1' to confirm your orders or press '2' to delete your orders or anykey to return! ");
			Scanner press3 = new Scanner(System.in);
			 int e= press3.nextInt();
			if(e==1){
				updateOrder();
			}
			if(e==2){
				deleteOrder();
			}
			costomer();
			break;
		case 6:
			queryOrder();
			costomer();
			break;
		case 7:
			run();
			break;
		default:
			break;
		}
	}

	private void queryOrder() {
		// TODO Auto-generated method stub
		orderAction o=new orderAction();
		o.get();
	}

	private void querycard() {
		// TODO Auto-generated method stub
		creditcardAction cd=new creditcardAction();
		cd.getcard();
	}

	private void updatecard() {
		// TODO Auto-generated method stub
		creditcardAction cd=new creditcardAction();
		cd.updatecard();
	}

	private void delcard() {
		// TODO Auto-generated method stub
		creditcardAction cd=new creditcardAction();
		cd.delcard();
	}

	private void addcard() {
		// TODO Auto-generated method stub
		creditcardAction cd=new creditcardAction();
		cd.addcard();
	}

	private void queryaddress() {
		// TODO Auto-generated method stub
		addressAction a=new addressAction();
		a.get();
	}

	private void updateaddress() {
		// TODO Auto-generated method stub
		addressAction a=new addressAction();
		a.update();
	}

	private void deleteOrder() {
		// TODO Auto-generated method stub
		orderAction o=new orderAction();
		o.delete();
	}

	private void Purchase() throws Exception {
		// TODO Auto-generated method stub
		orderAction o=new orderAction();
		o.create();
	}

	private void updateOrder() {
		// TODO Auto-generated method stub
		orderAction o=new orderAction();
		o.update();
	}

	private void getOrder() {
		// TODO Auto-generated method stub
		System.out.println("Here are your orders!");
		orderAction o=new orderAction();
		o.get();
	}

	private void Scan() {
		// TODO Auto-generated method stub
		System.out.println("here are the products' information");
		productAction p=new productAction();
		p.query();
	}

	private void  addaddress() {
		// TODO Auto-generated method stub
		addressAction a=new addressAction();
		a.add();
	
	}

	private void deladdress() {
		addressAction a=new addressAction();
		a.del();
		// TODO Auto-generated method stub
		
	}

	
	
	
	
	
	
}
